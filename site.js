const movieList = [
    {
      title: "Avatar",
      releaseYear: 2009,
      duration: 162,
      director: "James Cameron",
      actors: ["Sam Worthington","Zoe Saldana", "Sigourney Weaver", "Stephen Lang"],
      description: "A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home.",
      poster: "https://m.media-amazon.com/images/M/MV5BZDA0OGQxNTItMDZkMC00N2UyLTg3MzMtYTJmNjg3Nzk5MzRiXkEyXkFqcGdeQXVyMjUzOTY1NTc@._V1_SX300.jpg",
      rating: 7.9,
    },
    
    {
      title: "300",
      releaseYear: 2006,
      duration: 117,
      director: "Zack Snyder",
      actors: ["Gerard Butler", "Lena Headey", "Dominic West", "David Wenham"],
      description: "King Leonidas of Sparta and a force of 300 men fight the Persians at Thermopylae in 480 B.C.",
      poster: "https://m.media-amazon.com/images/M/MV5BMjc4OTc0ODgwNV5BMl5BanBnXkFtZTcwNjM1ODE0MQ@@._V1_SX300.jpg",
      rating: 7.7,
    },
    {
      title: "The Avengers",
      releaseYear: 2012,
      duration: 143,
      director: "Joss Whedon",
      actors: ["Robert Downey Jr.", "Chris Evans", "Mark Ruffalo", "Chris Hemsworth"],
      description: "Earth's mightiest heroes must come together and learn to fight as a team if they are to stop the mischievous Loki and his alien army from enslaving humanity.",
      poster: "https://m.media-amazon.com/images/M/MV5BNDYxNjQyMjAtNTdiOS00NGYwLWFmNTAtNThmYjU5ZGI2YTI1XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SX300.jpg",
      rating: 8.1,
    },
    {
        title: "Whiplash",
        releaseYear: 2014,
        duration: 147,
        director: "Damien Chazelle",
        actors: ["Miles Teller", " J.K Simmons"],
        description: "Whiplash depicts the intense relationship between an ambitious young drummer and a relentless music instructor, exploring themes of obsession, sacrifice, and the pursuit of greatness through ruthless mentorship.",
        poster: "https://m.media-amazon.com/images/W/MEDIAX_792452-T2/images/I/61JMxIPV-iL._AC_UF1000,1000_QL80_.jpg",
        rating: 8.5,
      },
  ];

  const movieToRemove = "The Avengers";
// la boucle  for nous permet d'afficher la liste des objets 
 for (let i = 0; i < movieList.length; i++) {
    const movie = movieList[i];
    console.log(`Film ${i + 1}:`);
    console.log(`Titre: ${movie.title}`);
    console.log(`Année de sortie: ${movie.releaseYear}`);
    console.log(`Durée: ${movie.duration} minutes`);
    console.log(`Réalisateur: ${movie.director}`);
    console.log(`Acteurs: ${movie.actors.join(", ")}`);
    console.log(`Description: ${movie.description}`);
    console.log(`Note: ${movie.rating}`);
    console.log(`Affiche: ${movie.poster}`);
    console.log("\n");

// cette  commande nous permet de trouver le film the avengers
const indexToRemove = movieList.findIndex((movie) => movie.title === "The Avengers");


// et celle ci nous permet de supprimer le film grace au -1
if (indexToRemove !== -1) {
    movieList.splice(indexToRemove, 1);
  }
   } 